

// default setup of express
const express = require('express')

// creation of an app/project
// in layman's terms, app is our server
const app = express()
const port = 3000

// setup for allowing the server to handle data from a request
// middlewares
app.use(express.json())


// allows your app to read data from forms
app.use(express.urlencoded({ extended: true }))


//[ROUTES]
app.get("/", (req, res) => {
	res.send("Hello World")
})

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})


let users = []

// POST METHOD
app.post('/signup', (req, res) => {
	console.log(req.body)

	const { username, password } = req.body

	if(!username || !password) {
		res.send('All Fields are required')
	} else {
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
	}
})


// PUT UPDATE METHOD
app.put("/change-password", (req, res) => {
	let message
	let userFound = false

	for(let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username) {
	        users[i].password = req.body.password;

	        message = `User ${req.body.username}'s password has been updated.`;
	        userFound = true;
	        break;
    	} 
	}

	if(!userFound) message = 'User does not exist'
	res.send(message)
})

// CODE ALONG ACTIVITY

// Home Page

app.get('/home', (req, res) => {
	res.send("Welcome to HomePage")
})

// Registered Users
app.get('/users', (req, res) => {
	res.send(users)
})

// DELETE USERS
app.delete('/delete-user', (req, res) => {
	let message;
	let userFound = false;

	
	for (let i = 0; i < users.length; i++) {
	    if(req.body.username == users[i].username) {
	        users.splice(i, 1);
	        message = `User ${req.body.username} has been deleted`;
	        userFound = true;
	        break;
	    } 
	}
	
	if(!userFound) message = "User does not exist.";
	res.send(message)
})



app.listen(port, () => console.log(`Server is running at port ${port}`))
